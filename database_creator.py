import mysql.connector
from mysql.connector import Error
import os
from dotenv import load_dotenv
from contextlib import contextmanager



class DatabaseCreator:
    def __init__(self):
        load_dotenv()
        self.host = os.getenv("HOST")
        self.port = os.getenv("PORT")
        self.username = os.getenv("USER")
        self.password = os.getenv("PASSWORD")
        self.database = os.getenv("DATABASE")

    def create_database(self):
        try:
            server_connect = mysql.connector.connect(host=self.host, port=self.port, username=self.username, password=self.password)
            cursor = server_connect.cursor()
            query = "CREATE DATABASE IF NOT EXISTS {}".format(self.database)
            cursor.execute(query)
            cursor.close()
            server_connect.close()
        except Error as error:
            print(f"Failed to create database {error}")
  
    @contextmanager
    def connect_to_database(self):
        try:
            db = mysql.connector.connect(
                host=self.host,
                port = self.port,
                user=self.username,
                password=self.password,
                database=self.database
            )
            yield db
        except Error as error:
            print(f"Failed to connect to database {error}")
        finally:
            db.close()

    def create_tables(self):
        with self.connect_to_database() as db:
            with db.cursor() as cursor:
                articles_query = """
                CREATE TABLE IF NOT EXISTS articles (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    title VARCHAR(255),
                    content TEXT,
                    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                )"""
                cursor.execute(articles_query)
                categories_query = """
                CREATE TABLE IF NOT EXISTS categories (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    name VARCHAR(255)
                    )
                    """
                cursor.execute(categories_query)
                article_category_query = """
                CREATE TABLE IF NOT EXISTS article_category (
                article_id INT,
                category_id INT,
                FOREIGN KEY (article_id) REFERENCES articles(id) ON DELETE CASCADE,
                FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE,
                PRIMARY KEY (article_id, category_id)
                )"""
                cursor.execute(article_category_query)


